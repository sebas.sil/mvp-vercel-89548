This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Local machine

When run this code on local machine it's fine and all works.

On server console show up the message `connected` and at browser shows `ok`.

To reproduce....

First, install the dependencies:

```bash
yarn install
```

Then, run the development server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Vercel

When run this code at Vercel machine it's not work.

On server console show up the message 
```text
[GET] /api/hello
09:00:43:61
error [Error: SQLITE_CANTOPEN: unable to open database file] {
  errno: 14,
  code: 'SQLITE_CANTOPEN'
}
2021-03-10T12:00:53.704Z f3e4294f-b2e4-4103-9ef7-a5f2c8a4fafe Task timed out after 10.01 seconds
```
and at browser shows nothing.

To reproduce....

```bash
vercel
```

Open the link given by the previous step with your browser to see the result.
