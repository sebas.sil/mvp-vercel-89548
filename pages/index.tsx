import {useEffect, useState} from 'react'

export default function Home() {

  const [status, setStatus] = useState<object>()
  
  useEffect(() => {
    fetch('api/hello').then(e =>{
      return e.json().then(e => e.msg)
    }).then(setStatus)
  }, [])
  return (
    <div>{status}</div>
  )
}
