import {NowRequest, NowResponse} from '@vercel/node'
import sqlite3 from 'sqlite3'
const db = new sqlite3.Database('hello.db3', (e) =>{
  if(e) {
    console.log('error', e)
  } else {
    console.log('connected')
  }
})

const Hello = async (request: NowRequest, response: NowResponse) => {

  return new Promise(function (resolve, reject) {

    try {
      let stmt = db.prepare('SELECT * FROM hello')
      stmt.run([], (err, rows) => {
      if (err) reject(err)
      else resolve('ok')
      })
    } catch (error) {
      response.status(500).json({msg: error.message})
    }
    
  }).then(e => response.status(200).json({msg: e})
  ).catch(e => response.status(400).json({msg: e}))
}

export default Hello
